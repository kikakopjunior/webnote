

export class JsonFileCreator{
    constructor(data){
        this.data = data;
    }
    createJsonString(){
        return JSON.stringify(this.data);
    }
    downloadJsonFile(filename){
        const jsonData = this.createJsonString();
        const blob = new Blob([jsonData], {type: 'application/json'});
        const url = URL.createObjectURL(blob);
        const linkFile = document.createElement('a');
        linkFile.href = url;
        linkFile.download = filename || 'data.json';
        linkFile.click();
        URL.revokeObjectURL(url);
    }
}