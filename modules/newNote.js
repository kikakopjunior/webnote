export const formData = {
    title: '',
    content: '',
    extension_file: '',
    file_name: '' // Nouveau champ pour le nom du fichier
};

function handleChange(event) {
    const { name, value } = event.target;
    formData[name] = value;
}

function resetForm() {
    formNewNote.reset();
    getErrorInput().forEach(element => element.textContent = '');
    for (let key in formData) {
        formData[key] = '';
    }
}

export function handleSubmit(event) {
    event.preventDefault();
    if (verifyEnter()) {
        const currentFormData = {...formData}; // Sauvegarde les données actuelles
        resetForm();
        return currentFormData; // Retourne les données sauvegardées
    } else {
        const formHtml = document.getElementById('error-form_newnote');
        formHtml.textContent = 'Erreur, veuillez vérifier vos saisies';
        return null; // Retourne null en cas d'erreur
    }
}


const title = document.getElementById('title');
const content = document.getElementById('content');
const extensionFile = document.getElementById('extension_file');
const fileName = document.getElementById('file_name'); // Nouvel input pour le nom du fichier
const formNewNote = document.getElementById('form_newnote');
const elementForm = [title, content, extensionFile, fileName]; // Ajout du nouvel input

export function getErrorInput() {
    return elementForm.map(item => document.getElementById('error-' + item.id));
}

export function verifyEnter() {
    const devErrors = getErrorInput();
    let hasError = false;

    for (let inputUser in formData) {
        const errorElement = devErrors.find(element => element.id === 'error-' + inputUser);
        if (formData[inputUser] === '') {
            if (errorElement) {
                errorElement.textContent = 'Le champ ne peut pas être vide';
                hasError = true;
            }
        } else if (errorElement) {
            errorElement.textContent = '';
        }
    }

    return !hasError;
}

export function initForm() {
    title.addEventListener('change', handleChange);
    content.addEventListener('change', handleChange);
    extensionFile.addEventListener('change', handleChange);
    fileName.addEventListener('change', handleChange); // Ajout de l'écouteur pour le nouvel input
}

export function getFormListener() {
    return formNewNote;
}
