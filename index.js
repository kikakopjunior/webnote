import { JsonFileCreator } from "./modules/createFile.js";
import { initForm, handleSubmit, getFormListener } from "./modules/newNote.js";


function deleteObject(object, targets){
    let objSave = {...object};
    for (let item of targets){
        delete objSave[item]
    }
    return objSave
}

document.addEventListener('DOMContentLoaded', () => {
    initForm();
    const formHtml = getFormListener();
    if (formHtml) {
        formHtml.addEventListener('submit', (event) => {
            const formData = handleSubmit(event);
            if (formData) {
                console.log('Formulaire validé :', formData);
                const fileCreator = new JsonFileCreator(deleteObject(formData, ['file_name', 'extension_file']))
                fileCreator.downloadJsonFile(formData.file_name)
            }
        });
    }
});
